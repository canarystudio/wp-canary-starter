<?php

namespace App;

// Use a static front page
$about = get_page_by_title( 'Sample Page' );
update_option( 'page_on_front', $about->ID );
update_option( 'show_on_front', 'page' );
