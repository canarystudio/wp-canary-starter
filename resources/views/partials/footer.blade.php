<footer class="footer-primary text-light bg-dark mt-5">
  <div class="container">
    @php(dynamic_sidebar('sidebar-footer'))
  </div>
</footer>
